<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::any('/', array('as'=>'home.index', 'uses'=> 'HomeController@index'));
Route::any('/test', array('as'=>'home.test', 'uses'=> 'HomeController@test'));
Route::any('/test2', array('as'=>'home.test2', 'uses'=> 'HomeController@test2'));

Route::get('/login', array('as'=>'login.index', 'uses'=> 'LoginController@index'));
Route::get('/logout', array('as'=>'login.logout', 'uses'=> 'LoginController@logout'));
Route::get('/loginTwitter', array('as'=>'login.twitter', 'uses'=> 'LoginController@loginWithTwitter'));

Route::get('/actor/search', array('as'=>'home.actor.search', 'uses'=> 'HomeController@actorSearch'));
Route::get('/movie/search', array('as'=>'home.movie.search', 'uses'=> 'HomeController@movieSearch'));


Route::get('/share/{responseHash}', array('as'=>'recast.test', 'uses'=> 'RecastController@share'));

Route::get('/s/{responseHash}', array('as'=>'recast.test2', 'uses'=> 'RecastController@share'));
Route::any('/r/{movieRecastHash}', array('as'=>'recast.pick', 'uses'=> 'RecastController@pick'));
Route::any('/random', array('as'=>'recast.random', 'uses'=> 'RecastController@random'));

Route::get('/tmdb/movie/get/{movieId}', array('as'=>'tmdb.movie.get', 'uses'=> 'HomeController@loadMovieFromTMDB'));
Route::get('/admin', array('as'=>'admin.index', 'uses'=> 'AdminController@index'));
Route::get('/admin/responses', array('as'=>'admin.responses', 'uses'=> 'AdminController@responses'));
Route::get('/admin/recasts', array('as'=>'admin.recasts', 'uses'=> 'AdminController@recasts'));
Route::any('/admin/recasts/new', array('as'=>'admin.recasts.new', 'uses'=> 'AdminController@recastsNew'));

Route::get('/recasts', array('as'=>'recasts', 'uses'=> 'RecastController@userRecasts'));
Route::any('/recasts/new', array('as'=>'recasts.new', 'uses'=> 'RecastController@userRecastNew'));
Route::get('/recasts/{movieRecastId}', array('as'=>'recast.view', 'uses'=> 'RecastController@userRecast'));
Route::any('/recasts/{movieRecastId}/edit', array('as'=>'recast.edit', 'uses'=> 'RecastController@editRecast'));


Route::any('/i/m/{imageHash}', array('as'=>'image.hash.movie', 'uses'=> 'ImageController@movieHandler'));
Route::any('/i/m/{imageHash}/{size}', array('as'=>'image.hash.movie.size', 'uses'=> 'ImageController@movieHandler'));
Route::any('/i/a/{imageHash}', array('as'=>'image.hash.actor', 'uses'=> 'ImageController@actorHandler'));
Route::any('/i/a/{imageHash}/{size}', array('as'=>'image.hash.actor.size', 'uses'=> 'ImageController@actorHandler'));
Route::any('/i/c/{imageHash}', array('as'=>'image.hash.character', 'uses'=> 'ImageController@characterHandler'));
Route::any('/i/c/{imageHash}/{size}', array('as'=>'image.hash.character.size', 'uses'=> 'ImageController@characterHandler'));
