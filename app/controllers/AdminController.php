<?php

class AdminController extends BaseController {


	/**
	 * The layout that should be used for admin index.
	 */
	protected $layout = 'layouts.master';
	
	
	public function index(){
		
		if(Auth::user() && Auth::user()->access >= 9){
			$this->layout->content = View::make('home.admin',array('user'=> Auth::user()));	
		} else {
			
		}
		
	}
	
	public function responses(){
		
		$movieRecastResponses = MovieRecastResponse::orderby('created_at','DESC')->get();

		
		if(Auth::user() && Auth::user()->access >= 9){
			$this->layout->content = View::make('home.admin.responses',array('movieRecastResponses' => $movieRecastResponses,'user'=> Auth::user()));	
		} else {
			
		}
		
	}
	
	public function recasts(){
		
		$movieRecasts = MovieRecast::where('is_public','=',1)->get();
		$movieRecasts = $movieRecasts->sortBy(function($movieRecast){
			return $movieRecast->movie->title;
		});
		//$movies = Movie::all()->orderBy('title');
		$movies = Movie::orderby('title')->get();
		
		if(Auth::user() && Auth::user()->access >= 9){
			$this->layout->content = View::make('home.admin.recasts',array('movies' => $movies, 'movieRecasts' => $movieRecasts,'user'=> Auth::user()));	
		} else {
			
		}
		
	}
	
	public function recastsNew(){

		if(Auth::user() && Auth::user()->access >= 9){
			if(Input::has('createResponse')){
				if(Input::has('movie') && Input::has('character')){
					$movie = Movie::find(Input::get('movie'));
					if($movie->id){
						$movieRecast = new MovieRecast;
						$movieRecast->hash = $movieRecast->generateHash();
						$movieRecast->movie_id = $movie->id;
						$movieRecast->user_id = Auth::user()->id;
						$movieRecast->is_public = 1;
						$movieRecast->save();
						
						foreach(Input::get('character') as $characterId => $val){
							$movieCharacter = MovieCharacter::find($characterId);
							if($movieCharacter->id && $movieCharacter->movie_id == $movie->id){
								$movieRecastCharacter = new MovieRecastCharacter;
								$movieRecastCharacter->movie_recast_id = $movieRecast->id;
								$movieRecastCharacter->movie_character_id = $movieCharacter->id;
								$movieRecastCharacter->save();
							}
						}

					}
				}
				
			}
			
			$this->layout->content = View::make('home.recast.new',array());	
		} else {
			
		}
		
	}

}