<?php

class RecastController extends BaseController {


	/**
	 * The layout that should be used for responses.
	 */
	protected $layout = 'layouts.master';
	
	
	public function index(){
		
		$movie = new Movie;
		$movie->getFromTMDB(550);
		
		$movieRecast = MovieRecast::find(1);
		
		$movie = $movieRecast->movie;
		$characters = $movieRecast->recastCharacters;
		
		
		if(! Auth::user()){
			$this->layout->content = View::make('home.recast.nouser',array('movieRecast'=> $movieRecast,'characters'=>$characters));	
		} else {
			if(Input::has('recast-vote')){
				$user = Auth::user();
				foreach(Input::get('hidden-actor') as $key => $actor_id){
					if($actor_id != ''){
						$movieRecastCharacter = MovieRecastCharacter::find($key);
						$actor = Actor::find($actor_id);
						$vote = new MovieRecastVote;
						$vote->movie_recast_id = $movieRecast->id;
						$vote->user_id = $user->id;
						$vote->movie_recast_character_id = $movieRecastCharacter->id;
						$vote->actor_id = $actor->id;
						$vote->save();
					}
				}
			}
			$this->layout->content = View::make('home.recast',array('movieRecast'=> $movieRecast, 'characters'=>$characters));
		}
		
	}

	public function share($responseHash){
		$movieRecastResponses = MovieRecastResponse::where('hash','=',$responseHash);
		
		if($movieRecastResponses->count() > 0){
			$movieRecastResponse = $movieRecastResponses->first();
			$movieRecast = $movieRecastResponse->recast;
			$movie = $movieRecast->movie;
			
			$votes = $movieRecastResponse->votes;
			$title = 'Check out my picks for recasting ' . $movie->title;
			
			$movieReleaseDate = new DateTime($movie->release_date);
			
			$this->layout->content = View::make(
				'home.recast.share',
				array(
					'movieReleaseDate' => $movieReleaseDate,
					'title' => $title,
					'user' => Auth::user(),
					'movie'=> $movie,
					'movieRecast'=> $movieRecast,
					'movieRecastResponse'=> $movieRecastResponse,
					'votes' => $votes
				)
			);
		
		} else {
			
		
		
		
		}
	}
	
	public function random(){
		
		$movieRecasts = MovieRecast::where('is_public','=',1)->orderBy(DB::raw('RAND()'))->get();
		$movieRecastFound = FALSE;
		foreach($movieRecasts as $movieRecast){
			if(! Auth::user()){
				$movieRecastFound = TRUE;
				break;	
			} else {
				if(!$movieRecast->hasVoted(Auth::user())){
					$movieRecastFound = TRUE;
					break;
				}
			}
		}
		
		return Redirect::to('r/'.$movieRecast->hash.'/');
			
	}
	
	public function pick($movieRecastHash){

		$movieRecast = MovieRecast::where('hash','=',$movieRecastHash)->firstOrFail();
		
		if($movieRecast->id == NULL)
			return false;

		$movie = $movieRecast->movie;
		$characters = $movieRecast->recastCharacters;
		
		$movieReleaseDate = new DateTime($movie->release_date);
		$doRecastGoal = false;
		
		if(! Auth::user()){
			if(Input::has('recast-vote')){
				$user = Auth::user();
				
				//Validate
				$movieRecastResponse  = new MovieRecastResponse;
				$movieRecastResponse->movie_recast_id = $movieRecast->id;
				$movieRecastResponse->session_id = Session::getId();
				$movieRecastResponse->hash = $movieRecastResponse->generateHash();
				$movieRecastResponse->save();
				$doRecastGoal = TRUE;
				
				foreach(Input::get('hidden-actor') as $key => $actor_id){
					if($actor_id != ''){
						$movieRecastCharacter = MovieRecastCharacter::find($key);
						$actor = Actor::find($actor_id);
						$vote = new MovieRecastResponseVote;
						$vote->movie_recast_response_id = $movieRecastResponse->id;
						$vote->movie_recast_character_id = $movieRecastCharacter->id;
						$vote->actor_id = $actor->id;
						$vote->save();
					}
				}
			}
			$movieRecastResponse = $movieRecast->getUserResponse(Auth::user());
			$this->layout->content = View::make(
				'home.recast',
				array(
					'movieReleaseDate' => $movieReleaseDate,
					'movie' => $movie,
					'movieRecast'=> $movieRecast, 
					'characters'=>$characters,
					'movieRecastResponse'=>$movieRecastResponse,
					'doRecastgoal' => $doRecastGoal
				)
			);
		} else {
			if(Input::has('recast-vote')){
				$user = Auth::user();
				
				//Validate
				$movieRecastResponse  = new MovieRecastResponse;
				$movieRecastResponse->movie_recast_id = $movieRecast->id;
				$movieRecastResponse->user_id = $user->id;
				$movieRecastResponse->session_id = Session::getId();
				$movieRecastResponse->hash = $movieRecastResponse->generateHash();
				$movieRecastResponse->save();
				$doRecastGoal = TRUE;
				
				foreach(Input::get('hidden-actor') as $key => $actor_id){
					if($actor_id != ''){
						$movieRecastCharacter = MovieRecastCharacter::find($key);
						$actor = Actor::find($actor_id);
						$vote = new MovieRecastResponseVote;
						$vote->movie_recast_response_id = $movieRecastResponse->id;
						$vote->movie_recast_character_id = $movieRecastCharacter->id;
						$vote->actor_id = $actor->id;
						$vote->save();
					}
				}
			}
			$movieRecastResponse = $movieRecast->getUserResponse(Auth::user());
			$this->layout->content = View::make(
				'home.recast',
				array(
					'movieReleaseDate' => $movieReleaseDate,
					'movie' => $movie,
					'movieRecast'=> $movieRecast, 
					'characters'=>$characters,
					'movieRecastResponse'=>$movieRecastResponse,
					'doRecastgoal' => $doRecastGoal
				)
			);
		}
		
	}
	
	public function userRecasts(){
		
		$movieRecasts = MovieRecast::where('user_id','=',Auth::user()->id)->where('is_public','=',0)->get();
		$movieRecasts = $movieRecasts->sortBy(function($movieRecast){
			return $movieRecast->movie->title;
		});
		//$movies = Movie::all()->orderBy('title');
		$movies = Movie::orderby('title')->get();

		if(Auth::user()){
			$this->layout->content = View::make('home.user.recasts',array('movies' => $movies, 'movieRecasts' => $movieRecasts,'user'=> Auth::user()));	
		} else {
			
		}
		
	}
	
	public function userRecast($movieRecastId){
		
		$movieRecast = MovieRecast::find($movieRecastId);
		$movie = $movieRecast->movie;
		$characters = $movieRecast->recastCharacters;
		$responses = $movieRecast->responses;
		
		$movieReleaseDate = new DateTime($movie->release_date);
		
		if(Auth::user() && ($movieRecast->user_id == Auth::user()->id || Auth::user()->access >= 9)){
			$this->layout->content = View::make('home.user.recast.view',array('movieReleaseDate' => $movieReleaseDate, 'movie' => $movie, 'movieRecast' => $movieRecast, 'characters'=>$characters, 'responses' => $responses,'user'=> Auth::user()));	
		} else {
			return Redirect::to('r/'.$movieRecast->hash.'/');
		}
		
	}
	
	public function editRecast($movieRecastId){
		
		$movieRecast = MovieRecast::find($movieRecastId);
		$movie = $movieRecast->movie;
		$characters = $movieRecast->recastCharacters;
		$responses = $movieRecast->responses;
		$movieCharacters = $movie->cast;
		$movieCharacterIds = array();
		foreach($characters as $character){
			$movieCharacterIds[] = $character->character->id;
		}
		
		if(Auth::user() && ($movieRecast->user_id == Auth::user()->id || Auth::user()->access >= 9)){
			if(Input::has('editResponse')){
				if(Input::has('character')){
					foreach($characters as $character){
						$character->delete();
					}
					$order = 0;
					foreach(Input::get('character') as $characterId => $val){
						$movieRecastCharacters = MovieRecastCharacter::withTrashed()->where('movie_recast_id','=',$movieRecast->id)->where('movie_character_id','=',$characterId)->get();
						if(sizeof($movieRecastCharacters) == 0){
							$movieCharacter = MovieCharacter::find($characterId);
							$movieRecastCharacter = new MovieRecastCharacter;
							$movieRecastCharacter->movie_recast_id = $movieRecast->id;
							$movieRecastCharacter->movie_character_id = $movieCharacter->id;
							$movieRecastCharacter->order = $order;
							$movieRecastCharacter->save();
							$order++;
						} else {
							$movieRecastCharacter = $movieRecastCharacters[0];
							$movieRecastCharacter->order = $order;
							$movieRecastCharacter->deleted_at = NULL;
							$movieRecastCharacter->save();
							$order++;	
						}
					}
					$movieRecast = MovieRecast::find($movieRecastId);
					$movie = $movieRecast->movie;
					$characters = $movieRecast->recastCharacters;
					$responses = $movieRecast->responses;
					$movieCharacters = $movie->cast;
					$movieCharacterIds = array();
					foreach($characters as $character){
						$movieCharacterIds[] = $character->character->id;
					}
				}
			}
			
			$this->layout->content = View::make('home.recast.edit',array('movieCharacterIds' => $movieCharacterIds, 'movie' => $movie, 'movieCharacters' => $movieCharacters, 'movieRecast' => $movieRecast, 'characters'=>$characters, 'responses' => $responses,'user'=> Auth::user()));	

		} else {
			
			return Redirect::to('r/'.$movieRecast->hash.'/');
			
		}

		
	}
	
	public function userRecastNew(){

		if(Auth::user()){
			if(Input::has('createResponse')){
				if(Input::has('movie') && Input::has('character')){
					$movie = Movie::find(Input::get('movie'));
					if($movie->id){
						$movieRecast = new MovieRecast;
						$movieRecast->hash = $movieRecast->generateHash();
						$movieRecast->movie_id = $movie->id;
						$movieRecast->user_id = Auth::user()->id;
						$movieRecast->is_public = 0;
						$movieRecast->save();
						
						$order = 0;
						foreach(Input::get('character') as $characterId => $val){
							$movieCharacter = MovieCharacter::find($characterId);
							if($movieCharacter->id && $movieCharacter->movie_id == $movie->id){
								$movieRecastCharacter = new MovieRecastCharacter;
								$movieRecastCharacter->movie_recast_id = $movieRecast->id;
								$movieRecastCharacter->movie_character_id = $movieCharacter->id;
								$movieRecastCharacter->order = $order;
								$movieRecastCharacter->save();
								$order++;
							}
						}
					
						return Redirect::to('recasts/'.$movieRecast->id.'/')->with('message','Movie recast created succesfully.')->with('message-type','success');

					}
				}
				
			}
			
			$this->layout->content = View::make('home.recast.new',array());	
		} else {
			
		}
		
	}
	
	

}