<?php

class ImageController extends BaseController {



	public function movieHandler($imageHash,$size = 'original'){
		
		$movie = Movie::where('hash','=',$imageHash)->firstOrfail();

		$file = public_path().'/images/movies/' . $movie->tmdb_id .'/poster.jpg';
		
		switch($size){
			
			case 'ac':
				$maxHeight = 60;
				$maxWidth = 60;
				break;
			
			case 'small':
				$maxHeight = 92;
				$maxWidth = 92;
				break;
				
			case 'medium':
				$maxHeight = 200;
				$maxWidth = 200;
				break;
				
			case 'share':
				$maxHeight = 200;
				$maxWidth = 260;
				break;
				
			case 'large' :
				$maxHeight = 400;
				$maxWidth = 400;
				break;
				
			default:
				break;	
		}
		
		if($size == 'original' || !isset($maxHeight) || !isset($maxWidth)){
			$type = 'image/jpeg';
			header('Content-Type:'.$type);
			header('Content-Length: ' . filesize($file));
			readfile($file);
		} else {
			$size = getimagesize($file);
			$ratioX = $maxWidth / $size[0];
			$ratioY = $maxHeight = $size[1];
			$ratio = min($ratioX,$ratioY);
			$width = $size[0]*$ratio;
			$height = $size[1]*$ratio;
			
			$data = array(
				'file' => $file,
				'width' => $maxWidth,
				'height' => $maxHeight
			);
			
			$img = Image::cache(function($image) use ($data) {
				return $image->make($data['file'])->resize($data['width'], $data['height'],true,true);
			},43200,true);

			return Response::make($img, 200, array('Content-Type' => 'image/jpeg'));
		}
		
		
		
		
		die();
		
	}
	
	public function actorHandler($imageHash,$size = 'original'){

		$actor = Actor::where('hash','=',$imageHash)->firstOrfail();
		
		$file = public_path().'/images/actors/' . $actor->tmdb_id .'/profile.jpg';
		if(!file_exists($file) || filesize($file) == 29)
			$file = public_path().'/images/actors/missing-headshot.jpg';
			
		switch($size){
			
			case 'ac':
				$maxHeight = 60;
				$maxWidth = 60;
				break;
			
			case 'small':
				$maxHeight = 100;
				$maxWidth = 100;
				break;
				
			case 'medium':
				$maxHeight = 200;
				$maxWidth = 200;
				break;
			
			case 'share':
				$maxHeight = 200;
				$maxWidth = 260;
				break;
				
			case 'large' :
				$maxHeight = 400;
				$maxWidth = 400;
				break;
				
			default:
				break;	
		}
		
		if($size == 'original' || !isset($maxHeight) || !isset($maxWidth)){
			$type = 'image/jpeg';
			header('Content-Type:'.$type);
			header('Content-Length: ' . filesize($file));
			readfile($file);
		} else {
			
			
			
			$sizeOriginal = $size;
			$size = getimagesize($file);
			$ratioX = $maxWidth / $size[0];
			$ratioY = $maxHeight = $size[1];
			$ratio = min($ratioX,$ratioY);
			$width = $size[0]*$ratio;
			$height = $size[1]*$ratio;

			$data = array(
				'file' => $file,
				'width' => $maxWidth,
				'height' => $maxHeight
			);
			
			$img = Image::cache(function($image) use ($data) {
				return $image->make($data['file'])->resize($data['width'], $data['height'],true,true);
			},1440,true);

			return Response::make($img, 200, array('Content-Type' => 'image/jpeg'));

		}
		
		die();
		
	}


	public function characterHandler($imageHash,$size = 'original'){
		
		$character = MovieCharacter::where('hash','=',$imageHash)->firstOrfail();

		$file = public_path().'/images/actors/' . $character->actor->tmdb_id .'/profile.jpg';
		if(!file_exists($file) || filesize($file) == 29)
			$file = public_path().'/images/actors/missing-headshot.jpg';
		
		switch($size){
			
			case 'ac':
				$maxHeight = 60;
				$maxWidth = 60;
				break;
			
			case 'small':
				$maxHeight = 100;
				$maxWidth = 100;
				break;
				
			case 'medium':
				$maxHeight = 200;
				$maxWidth = 200;
				break;
				
			case 'share':
				$maxHeight = 200;
				$maxWidth = 260;
				break;
				
			case 'large' :
				$maxHeight = 400;
				$maxWidth = 400;
				break;
				
			default:
				break;	
		}
		
		if($size == 'original' || !isset($maxHeight) || !isset($maxWidth)){
			$type = 'image/jpeg';
			header('Content-Type:'.$type);
			header('Content-Length: ' . filesize($file));
			readfile($file);
		} else {
			$size = getimagesize($file);
			$ratioX = $maxWidth / $size[0];
			$ratioY = $maxHeight = $size[1];
			$ratio = min($ratioX,$ratioY);
			$width = $size[0]*$ratio;
			$height = $size[1]*$ratio;
			
			$data = array(
				'file' => $file,
				'width' => $maxWidth,
				'height' => $maxHeight
			);
			
			$img = Image::cache(function($image) use ($data) {
				return $image->make($data['file'])->resize($data['width'], $data['height'],true,true);
			},1440,true);

			return Response::make($img, 200, array('Content-Type' => 'image/jpeg'));
		}
		
		die();
		
	}

}