<?php

class HomeController extends BaseController {


	/**
	 * The layout that should be used for responses.
	 */
	protected $layout = 'layouts.master';
	
	
	public function index(){
		
		if(Session::has('loginRedirect')){
			$redirect = Session::get('loginRedirect');
			Session::forget('loginRedirect');
			header('Location: ' . $redirect);
			die();
		}


		if(Input::has('recast-vote')){
			$movieRecastFound = TRUE;
			$movieRecast = MovieRecast::find(Input::get('recast-vote'));
		} else {
			//$movieRecast = MovieRecast::where('is_public','=',1)->orderBy('created_at','DESC')->first();
			$movieRecasts = MovieRecast::where('is_public','=',1)->orderBy('created_at','DESC')->get();
			$movieRecastFound = FALSE;
			foreach($movieRecasts as $movieRecast){
				if(! Auth::user()){
					$movieRecastFound = TRUE;
					break;	
				} else {
					if(!$movieRecast->hasVoted(Auth::user())){
						$movieRecastFound = TRUE;
						break;
					}
				}
			}
		
		}
		
		if(!$movieRecastFound){
			echo 'TODO: All movie recasts have been voted on by this user, show them a message';	
		}
		
		$movie = $movieRecast->movie;
		$characters = $movieRecast->recastCharacters;
		
		$movieReleaseDate = new DateTime($movie->release_date);
		$doRecastGoal = FALSE;
		if(! Auth::user()){
			if(Input::has('recast-vote')){
				$user = Auth::user();
				
				//Validate
				$movieRecastResponse  = new MovieRecastResponse;
				$movieRecastResponse->movie_recast_id = $movieRecast->id;
				$movieRecastResponse->session_id = Session::getId();
				$movieRecastResponse->hash = $movieRecastResponse->generateHash();
				$movieRecastResponse->save();
				$doRecastGoal = TRUE;
				
				foreach(Input::get('hidden-actor') as $key => $actor_id){
					if($actor_id != ''){
						$movieRecastCharacter = MovieRecastCharacter::find($key);
						$actor = Actor::find($actor_id);
						$vote = new MovieRecastResponseVote;
						$vote->movie_recast_response_id = $movieRecastResponse->id;
						$vote->movie_recast_character_id = $movieRecastCharacter->id;
						$vote->actor_id = $actor->id;
						$vote->save();
					}
				}
			}
			$movieRecastResponse = $movieRecast->getUserResponse(Auth::user());
			$this->layout->content = View::make(
				'home.recast',
				array(
					'movieReleaseDate' => $movieReleaseDate,
					'movie' => $movie,
					'movieRecast'=> $movieRecast, 
					'characters'=>$characters,
					'movieRecastResponse'=>$movieRecastResponse,
					'doRecastgoal' => $doRecastGoal
				)
			);
		} else {
			if(Input::has('recast-vote')){
				$user = Auth::user();
				
				//Validate
				$movieRecastResponse  = new MovieRecastResponse;
				$movieRecastResponse->movie_recast_id = $movieRecast->id;
				$movieRecastResponse->user_id = $user->id;
				$movieRecastResponse->session_id = Session::getId();
				$movieRecastResponse->hash = $movieRecastResponse->generateHash();
				$movieRecastResponse->save();
				$doRecastGoal = TRUE;
				
				foreach(Input::get('hidden-actor') as $key => $actor_id){
					if($actor_id != ''){
						$movieRecastCharacter = MovieRecastCharacter::find($key);
						$actor = Actor::find($actor_id);
						$vote = new MovieRecastResponseVote;
						$vote->movie_recast_response_id = $movieRecastResponse->id;
						$vote->movie_recast_character_id = $movieRecastCharacter->id;
						$vote->actor_id = $actor->id;
						$vote->save();
					}
				}
			}
			$movieRecastResponse = $movieRecast->getUserResponse(Auth::user());
			$this->layout->content = View::make(
				'home.recast',
				array(
					'movieReleaseDate' => $movieReleaseDate,
					'movie' => $movie,
					'movieRecast'=> $movieRecast, 
					'characters'=>$characters,
					'movieRecastResponse'=>$movieRecastResponse,
					'doRecastgoal' => $doRecastGoal
				)
			);
		}
		
	}

	public function loadMovieFromTMDB($movieId){
		$movie = new Movie;
		$movie->getFromTMDB($movieId);
		die();
	}

	public function test(){
		
		/*$actors = Actor::all();

		foreach($actors as $actor){
			
			if(!file_exists(public_path().'/images/actors/'.$actor->tmdb_id.'/profile.jpg')){

				echo $actor->name ."\n"; 
				$api_key = 'a3dc111e66105f6387e99393813ae4d5';
				$url = "https://api.themoviedb.org/3/person/$actor->tmdb_id?api_key=$api_key";
				$curl = new Curl;
				
				$curl->create($url);
				$curl->get();
				$json = $curl->execute();
				
				$actor_array = json_decode($json,true);
				print_r($actor_array);
				
				if (!file_exists(public_path().'/images/actors/'.$actor_array['id'])) {
					mkdir(public_path().'/images/actors/'.$actor_array['id'], 0777, true);
				}
	
				$file = public_path().'/images/actors/'.$actor_array['id'].'/profile.jpg';
				$file_remote = 'http://image.tmdb.org/t/p/original' .$actor_array['profile_path'];
				echo $file_remote;
				$headers = array();
				
				$ch = curl_init($file_remote);
				$fp = fopen($file, 'wb');
				curl_setopt($ch, CURLOPT_FILE, $fp);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_FTP_CREATE_MISSING_DIRS, true);
				curl_exec($ch);
				curl_close($ch);
				fclose($fp);
				
			
			}
				
		}
		*/
		
		/*$movies = MovieCharacter::where('hash','=',null)->get();
		foreach($movies as $movie){
			$movie->hash = $movie->generateHash();
			$movie->save();
		}*/
			
		
		$movie = new Movie;
		//$movie->getFromTMDBVoteCount(10);
		
		$movie->getFromTMDB(4688);
		
		/*
		$attempts = 0;
		$count = 0;
		$min = 1;
		$max = 1000;
		$movie = new Movie;
		
		$movie_id = 2200;
		while($attempts < 201){
			$attempts++;
			
			//$movie_id = mt_rand($min,$max);
			echo '<p>' . $movie_id. '</p>';
			if($movie->getFromTMDB($movie_id)){
				$count++;
				sleep(mt_rand(1,5));
			}
			$movie_id++;
			
				
		}
		
		echo $count;*/
	
		
	}


	public function test2(){
		$api_key = 'a3dc111e66105f6387e99393813ae4d5';
		foreach(Movie::all() as $movie){
			
			if($movie->overview == ''){
				$movie_id = $movie->tmdb_id;
				
				$url = "https://api.themoviedb.org/3/movie/$movie_id?api_key=$api_key";
				
				$curl = new Curl;
				
				$curl->create($url);
				$curl->get();
				$json = $curl->execute();
				
				$movie_array = json_decode($json,true);
				$movie->overview = $movie_array['overview'];
				$movie->save();
			}
			
		}
		
		
		
	}
	
	
	public function actorSearch(){
		
		$actors = Actor::where('name','LIKE','%'.$_GET['term'].'%')->orderby('name')->get();
		
		return Response::json($actors);
	}

	public function movieSearch(){
		
		$movies = Movie::with('cast','cast.actor')->where('title','LIKE','%'.$_GET['term'].'%')->orderby('title')->get();
		
		return Response::json($movies);
	}
}