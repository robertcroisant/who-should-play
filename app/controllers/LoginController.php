<?php
use OAuth\OAuth1\Service\Twitter;
use OAuth\Common\Storage\Session;
use OAuth\Common\Consumer\Credentials;
class LoginController extends BaseController {


	/**
	 * The layout that should be used for responses.
	 */
	protected $layout = 'layouts.login';
	
	
	public function index(){
		\Session::put('loginRedirect',$_SERVER['HTTP_REFERER']);
		return Redirect::route( 'login.twitter' );
		
	}
	
	public function logout(){
		
		if(Auth::check())
			Auth::logout();
		
		$message = "You have succesfully logged out";
		
		return Redirect::route( 'home.index' )
			->with( 'flash_success', $message );
		
	}

	public function loginWithTwitter2() {
		
		
		$uriFactory = new \OAuth\Common\Http\Uri\UriFactory();
		$currentUri = $uriFactory->createFromSuperGlobalArray($_SERVER);
		$currentUri->setQuery('');
		
		$servicesCredentials = array(
			'amazon' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'bitbucket' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'bitly' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'box' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'dailymotion' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'dropbox' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'etsy' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'facebook' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'fitbit' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'flickr' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'foursquare' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'github' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'google' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'instagram' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'linkedin' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'mailchimp' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'microsoft' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'paypal' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'reddit' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'runkeeper' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'soundcloud' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'tumblr' => array(
				'key'	   => '',
				'secret'	=> '',
			),
			'twitter' => array(
				'key' 			=> 'psTGUr1qDrqsYQQeHmGw',
				'secret'		=> 'HndocdUqHJEBVWzNfmEAGe4CDE8s1KlhQ6CDVbvFWQ',
			),
			'yammer' => array(
				'key'	   => '',
				'secret'	=> ''
			),
		);
		
		/** @var $serviceFactory \OAuth\ServiceFactory An OAuth service factory. */
		$serviceFactory = new \OAuth\ServiceFactory();
		/**
		 * Bootstrap the example
		 */

		
		// We need to use a persistent storage to save the token, because oauth1 requires the token secret received before'
		// the redirect (request token request) in the access token request.
		$storage = new Session();
		
		// Setup the credentials for the requests
		$credentials = new Credentials(
			$servicesCredentials['twitter']['key'],
			$servicesCredentials['twitter']['secret'],
			$currentUri->getAbsoluteUri()
		);
		
		// Instantiate the twitter service using the credentials, http client and storage mechanism for the token
		/** @var $twitterService Twitter */
		$twitterService = $serviceFactory->createService('twitter', $credentials, $storage);
		
		if (!empty($_GET['oauth_token'])) {
			$token = $storage->retrieveAccessToken('Twitter');
		
			// This was a callback request from twitter, get the token
			$twitterService->requestAccessToken(
				$_GET['oauth_token'],
				$_GET['oauth_verifier'],
				$token->getRequestTokenSecret()
			);
		
			// Send a request now that we have access token
			$result = json_decode($twitterService->request('account/verify_credentials.json'));
		
			echo 'result: <pre>' . print_r($result, true) . '</pre>';
		
		} elseif (!empty($_GET['go']) && $_GET['go'] === 'go') {
			// extra request needed for oauth1 to request a request token :-)
			$token = $twitterService->requestRequestToken();
		
			$url = $twitterService->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));

			header('Location: ' . $url);
			die();
		} else {
			$url = $currentUri->getRelativeUri() . '?go=go';
			echo "<a href='$url'>Login with Twitter!</a>";
		}
	}
	
	/**
	 * Login user
	 *
	 * @return void
	 */
	public function loginWithTwitter() {

			// get data from input
			$code = Input::get( 'oauth_token' );
			$oauth_verifier = Input::get( 'oauth_verifier' );

			// get twitter service
			$twitterService = Artdarek\OAuth\Facade\OAuth::consumer( 'Twitter' );
			
			// check if code is valid

			// if code is provided get user data and sign in
			if ( !empty( $code ) ) {

				$token = $twitterService->getStorage()->retrieveAccessToken('Twitter');
				
				// This was a callback request from google, get the token
				$twitterService->requestAccessToken( $code, $oauth_verifier, $token->getRequestTokenSecret() );
				
				// Send a request with it
				$result = json_decode( $twitterService->request( 'account/verify_credentials.json') );
				// try to login

				// get user by twitter_id
				$user = User::where('twitter_id', '=', $result->id)->first();
				
				// check if user exists
				if ( $user ) {
						// login user
						Auth::login( $user );
						$event = Event::fire('user.login', array($user));

						// build message with some of the resultant data
						$message = 'Your unique twitter user id is: ' . $result->id . ' and your name is ' . $result->name;

						// redirect to user profile
						return Redirect::route( 'home.index' )
								->with( 'flash_success', $message );

				}
				else {
						// FIRST TIME TWITTER LOGIN

						// create new user
						$user = new User;
						$user->twitter_id = $result->id;
						
						$user->save();
						
						$twitter = new TwitterAccount;
						$twitter->id = $result->id;
						$twitter->name = $result->name;
						$twitter->screen_name = $result->screen_name;
						$twitter->location = $result->location;
						$twitter->description = $result->description;
						$twitter->url = $result->url;
						$twitter->followers_count = $result->followers_count;
						$twitter->statuses_count = $result->statuses_count;
						$twitter->lang = $result->lang;
						$twitter->time_zone = $result->time_zone;
						$twitter->twitter_created = $result->created_at;
						$twitter->save();

						// login user
						Auth::login( $user );
						$event = Event::fire('user.login', array($user));

						// build message with some of the resultant data
						$message_success = 'Your unique twitter user id is: ' . $result->id . ' and your name is ' . $result->name;
						$message_notice = 'Account Created.';

						// redirect to game page
						return Redirect::route( 'home.index' )
								->with( 'flash_success', $message_success )
								->with( 'flash_notice', $message_notice );

				}
			}
			// if not ask for permission first
			else {

				
				// extra request needed for oauth1 to request a request token :-)
				$token = $twitterService->requestRequestToken();
				$url = $twitterService->getAuthorizationUri(array('oauth_token' => $token->getRequestToken()));

					// return to twitter login url
				return Response::make()->header( 'Location', (string)$url );

			}

		}
	

}