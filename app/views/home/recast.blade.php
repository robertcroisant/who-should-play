@section('meta')
			
		<meta property="og:site_name" content="Who Should Play" />
		<meta property="og:title" content="{{ $movie->title }} [{{ $movieReleaseDate->format('Y') }}] :: Who Should Play" />
		<title>{{ $movie->title }} [{{ $movieReleaseDate->format('Y') }}] :: Who Should Play</title>

@endsection

@section('highlighted')

<div class="highlighted">
	<div class="content">
		<div class="recast-movie clearfix">
			<div class="recast-movie-left">
				@if(!in_array($movie->asin,array(NULL,'')))<a href="http://www.amazon.com/gp/product/{{ $movie->asin }}/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN={{ $movie->asin }}&linkCode=as2&tag=whoshopla-20"><img class="recast-poster img-rounded" src="http://whoshouldplay.com/i/m/{{ $movie->hash }}/medium" alt="{{ $movie->title }}" /></a>@else<img class="recast-poster img-rounded" src="http://whoshouldplay.com/i/m/{{ $movie->hash }}/medium" alt="{{ $movie->title }}" />@endif
				@if(!in_array($movie->asin,array(NULL,'')))<a class="btn btn-info btn-small" href="http://www.amazon.com/gp/product/{{ $movie->asin }}/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN={{ $movie->asin }}&linkCode=as2&tag=whoshopla-20">Watch this movie</a>@endif 
			</div>
			<div class="recast-movie-details">
				<div class="recast-movie-title">
					{{ $movie->title }} @if(isset($movieReleaseDate) && $movieReleaseDate) [{{ $movieReleaseDate->format('Y') }}] @endif
				</div>
				<div class="recast-movie-overview">
					{{ $movie->overview }}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('content')
	<script type="text/javascript" >
		$(document).ready(function() {
			var actorCache = {};
			
			
			
			var autoc = {
				minLength: 2,
				source: function( request, response ) {
					var term = request.term;
					if ( term in actorCache ) {
						data = actorCache[ term ];
						//response( actorCache[ term ] );
						response($.map(data.slice(0, 5), function (item) {
							return { label: item.name, id: item.id, image: "http://whoshouldplay.com/i/a/"+item.hash+"/ac", description: 'Actor' };
						}));
					} else {
						$.getJSON( "/actor/search", request, function( data, status, xhr ) {
							actorCache[ term ] = data;
							//response( data );
							response($.map(data.slice(0, 5), function (item) {
								return { label: item.name, id: item.id, image: "http://whoshouldplay.com/i/a/"+item.hash+"/ac", description: 'Actor' };
							}));
						});
					}
				},
				select: function( event, ui ) {
					$element = $(".recast-character .choice input[name='hidden-" + event.target.name + "']");
					$element.val(ui.item.id);
					
					$(".recast-character .choice input[name='" + event.target.name + "']").prop('readonly',true);
					
					$neededVotes = $(".recast-character .choice input[type=hidden][value='']").length;
					if($neededVotes > 0){
						$('button[name=recast-vote]').prop('disabled',true);
					} else {
						$('button[name=recast-vote]').prop('disabled',false);
					}
				}
			};
			
			var renderItem = function( ul, item ) {
				var inner_html = '<a><div class="list_item_container"><div class="image"><img src="' + item.image + '"></div><div class="name">' + item.label + '</div><div class="description">' + item.description + '</div></div></a>';
				return $( "<li></li>" )
					.data( "item.autocomplete", item )
					.append(inner_html)
					.appendTo( ul );
			};
			
			$(".recast-character input[type=text]").each(function (i) {
			     $(this).autocomplete(autoc).data("ui-autocomplete")._renderItem = renderItem;
			});
			
			$('.actor-clear').on('click',function(){
				$(this).closest('.form-control').val('');
				$(this).closest('.form-control').prop('readonly',false);
			});
			
			
			
			
		});
		
		function actorClear($characterId){
				$element = $(".recast-character .choice input[name='actor[" + $characterId + "]']");
				$element.val('');
				$element.prop('readonly',false);
				$(".recast-character .choice input[name='hidden-actor[" + $characterId + "]']").val('');
				$neededVotes = $(".recast-character .choice input[type=hidden][value='']").length;
				if($neededVotes > 0){
					$('button[name=recast-vote]').prop('disabled',true);
				} else {
					$('button[name=recast-vote]').prop('disabled',false);
				}
			}
	</script>
	
	<div class="recast-banner adsense">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- WSP - Responsive -->
		<ins class="adsbygoogle wsp-responsive"
		     style="display:inline-block"
		     data-ad-client="ca-pub-0338988042910939"
		     data-ad-slot="9557691497"></ins>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>
	
	<form role="form" class="movie-recast" action="" method="POST" >
		
	@foreach($characters as $character)
		@if (isset($movieRecastResponse->id) && $movieRecastResponse->id != null)
		<div class="recast-character clearfix">
			<img class="photo img-rounded img-responsive" src="http://whoshouldplay.com/i/c/{{ $character->character->hash }}/small" />
			<div class="text">
				<div class="name">{{ $character->character->name }} ({{ $character->character->actor->name }})</div>
				<div class="choice">
					<label class="sr-only" for="actor">Actor Name</label>
					<input type="text"  value="{{ $movieRecastResponse->getCharacterVote($character)->actor->name }}" class="form-control" placeholder="Actor name" disabled="disabled"/>
				</div>
			</div>
		</div>
		@else
		<div class="recast-character clearfix">
			<img class="photo img-rounded img-responsive" src="http://whoshouldplay.com/i/c/{{ $character->character->hash }}/small" />
			<div class="text">
				<div class="name">{{ $character->character->name }} ({{ $character->character->actor->name }})</div>
				<div class="choice">
					<label class="sr-only" for="actor">Actor Name</label>
					<input type="hidden" name="hidden-actor[{{ $character->id }}]" value="" />
					<div class="input-group">
						<input type="text" name="actor[{{ $character->id }}]" value="" class="form-control" placeholder="Actor name" />
						<span class="input-group-btn">
							<button class="btn btn-default actor-clear" onclick="return actorClear({{ $character->id }});" type="button">X</button>
						</span>
				    </div><!-- /input-group -->
					
				</div>
			</div>
		</div>
		@endif
	@endforeach
		
		<div class="recast-banner banner-728">
			<iframe src="http://rcm-na.amazon-adsystem.com/e/cm?t=whoshopla-20&o=1&p=48&l=ur1&category=amazonvideoondemand&banner=0NV7S8PAP1Y8DYFGJC82&f=ifr" width="728" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>
		</div>
		<div class="recast-banner banner-300">
			<iframe src="http://rcm-na.amazon-adsystem.com/e/cm?t=whoshopla-20&o=1&p=12&l=ur1&category=amazonvideoondemand&banner=1B2BTR6P751PMAH984G2&f=ifr" width="300" height="250" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>
		</div>
		
		<div class="recast-submit">
		@if ($movieRecastResponse)	
			<button type="submit" class="btn btn-primary btn-lg" name="recast-vote" value="{{ $movieRecast->id }}" disabled="disabled">Submit</button>
		@else
			<button type="submit" class="btn btn-primary btn-lg" name="recast-vote" value="{{ $movieRecast->id }}" disabled="disabled">Submit</button>
		@endif
		</div>
		
	</form>
@stop