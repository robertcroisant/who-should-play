@section('meta')
			
		<meta property="og:site_name" content="Who Should Play" />
		<meta property="og:title" content="Admin :: Who Should Play" />
		<title>Admin :: Who Should Play</title>

@endsection

@section('content')
	<p><a href="/admin/recasts/">Manage Active Recasts</a></p>
	<p><a href="/admin/responses/">Raw Recast Response Audit Logs</a></p>
	{{ Session::getId(); }}
@stop