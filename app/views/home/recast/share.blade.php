@section('meta')
			
		<!--<meta property="fb:app_id" content="" />-->
		<meta property="og:site_name" content="Who Should Play" />
		<!--<meta property="og:url" content="" />-->
		<meta property="og:title" content="{{ $title }} [{{ $movieReleaseDate->format('Y') }}]" />
		<meta property="og:description" content="What do you think of my picks?" />
		<meta property="og:image" content="{{ $movie->poster_link }}" />
		<title>{{ $title }} [{{ $movieReleaseDate->format('Y') }}]</title>

@endsection

@section('highlighted')

<div class="highlighted">
	<div class="content">
		<div class="recast-movie clearfix">
			<div class="recast-movie-left">
				@if(!in_array($movie->asin,array(NULL,'')))<a href="http://www.amazon.com/gp/product/{{ $movie->asin }}/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN={{ $movie->asin }}&linkCode=as2&tag=whoshopla-20"><img class="recast-poster img-rounded" src="http://whoshouldplay.com/i/m/{{ $movie->hash }}/medium" alt="{{ $movie->title }}" /></a>@else<img class="recast-poster img-rounded" src="http://whoshouldplay.com/i/m/{{ $movie->hash }}/medium" alt="{{ $movie->title }}" />@endif
				@if(!in_array($movie->asin,array(NULL,'')))<a class="btn btn-info btn-small" href="http://www.amazon.com/gp/product/{{ $movie->asin }}/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN={{ $movie->asin }}&linkCode=as2&tag=whoshopla-20">Watch this movie</a>@endif 
			</div>
			<div class="recast-movie-details">
				<div class="recast-movie-title">
					{{ $movie->title }} @if(isset($movieReleaseDate) && $movieReleaseDate) [{{ $movieReleaseDate->format('Y') }}] @endif
				</div>
				<div class="recast-movie-overview">
					{{ $movie->overview }}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('content')

	<div class="recast-banner adsense">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- WSP - Responsive -->
		<ins class="adsbygoogle wsp-responsive"
		     style="display:inline-block"
		     data-ad-client="ca-pub-0338988042910939"
		     data-ad-slot="9557691497"></ins>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>

	<div class="movie-recast response" >
		
	@foreach($votes as $vote)
		<div class="recast-vote clearfix">
			<div class="person">
				<div class="photo-container">
					<img class="photo img-rounded img-responsive" src="http://whoshouldplay.com/i/a/{{ $vote->actor->hash }}/share/" />
				</div>
				<div class="name">{{ $vote->actor->name }}</div>
			</div>
			<div class="as">as</div>
			<div class="person">
				<div class="photo-container">
					<img class="photo img-rounded img-responsive" src="http://whoshouldplay.com/i/c/{{ $vote->character->character->hash }}/share/" />
				</div>
				<div class="name">{{ $vote->character->character->name }}<br />({{ $vote->character->character->actor->name }})</div>
			</div>
		</div>
	@endforeach
	
		<div class="recast-banner banner-728">
			<iframe src="http://rcm-na.amazon-adsystem.com/e/cm?t=whoshopla-20&o=1&p=48&l=ur1&category=amazonvideoondemand&banner=0NV7S8PAP1Y8DYFGJC82&f=ifr" width="728" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>
		</div>
		<div class="recast-banner banner-300">
			<iframe src="http://rcm-na.amazon-adsystem.com/e/cm?t=whoshopla-20&o=1&p=12&l=ur1&category=amazonvideoondemand&banner=1B2BTR6P751PMAH984G2&f=ifr" width="300" height="250" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>
		</div>
	
	@if($user && $user->id == $movieRecastResponse->user_id )
		<div class="recast-response-social clearfix">
			<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://whoshouldplay.com/share/{{ $movieRecastResponse->hash }}" data-text="Check out my picks for recasting {{ $movie->title }}!" data-via="whoshouldplay" data-size="large" data-related="whoshouldplay" data-count="none">Tweet</a> <span> or </span> <a href="https://www.facebook.com/sharer/sharer.php?u=http://whoshouldplay.com/share/{{ $movieRecastResponse->hash }}" target="_blank" class="facebook"></a> <span>your picks!</span>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s) [0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	
		</div>
	
	@else
		<div class="text-center">
			<a class="btn btn-primary btn-lg" href="http://whoshouldplay.com/r/{{ $movieRecast->hash }}/">Who would you cast?</a>
		</div>

	@endif
	
		
	</div>
@stop