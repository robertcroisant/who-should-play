@section('meta')
			
		<meta property="og:site_name" content="Who Should Play" />
		<meta property="og:title" content="{{ $movie->title }} [{{ $movieReleaseDate->format('Y') }}] :: Who Should Play" />
		<title>{{ $movie->title }} [{{ $movieReleaseDate->format('Y') }}] :: Who Should Play</title>

@endsection

@section('highlighted')

<div class="highlighted">
	<div class="content">
		<div class="recast-movie clearfix">
			<div class="recast-movie-left">
				@if(!in_array($movie->asin,array(NULL,'')))<a href="http://www.amazon.com/gp/product/{{ $movie->asin }}/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN={{ $movie->asin }}&linkCode=as2&tag=whoshopla-20"><img class="recast-poster img-rounded" src="http://whoshouldplay.com/i/m/{{ $movie->hash }}/medium" alt="{{ $movie->title }}" /></a>@else<img class="recast-poster img-rounded" src="http://whoshouldplay.com/i/m/{{ $movie->hash }}/medium" alt="{{ $movie->title }}" />@endif
				@if(!in_array($movie->asin,array(NULL,'')))<a class="btn btn-info btn-small" href="http://www.amazon.com/gp/product/{{ $movie->asin }}/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN={{ $movie->asin }}&linkCode=as2&tag=whoshopla-20">Watch this movie</a>@endif 
			</div>
			<div class="recast-movie-details">
				<div class="recast-movie-title">
					{{ $movie->title }} @if(isset($movieReleaseDate) && $movieReleaseDate) [{{ $movieReleaseDate->format('Y') }}] @endif
				</div>
				<div class="recast-movie-overview">
					{{ $movie->overview }}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('content')

	<div class="recast-banner adsense">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- WSP - Responsive -->
		<ins class="adsbygoogle wsp-responsive"
		     style="display:inline-block"
		     data-ad-client="ca-pub-0338988042910939"
		     data-ad-slot="9557691497"></ins>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>

	<form role="form" class="movie-recast" >
		
	@foreach($characters as $character)
		<div class="recast-character clearfix">
			<img class="photo img-rounded img-responsive" src="http://whoshouldplay.com/i/c/{{ $character->character->hash }}/small" />
			<div class="text">
				<div class="name">{{ $character->character->name }} ({{ $character->character->actor->name }})</div>
				<div class="choice">
					<label class="sr-only" for="actor">Actor Name</label>
					<input type="text"  value="Log in to vote" class="form-control" placeholder="Actor name" disabled="disabled" /> 
				</div>
			</div>
		</div>
	@endforeach
		
		<div class="recast-banner banner-728">
			<iframe src="http://rcm-na.amazon-adsystem.com/e/cm?t=whoshopla-20&o=1&p=48&l=ur1&category=amazonvideoondemand&banner=0NV7S8PAP1Y8DYFGJC82&f=ifr" width="728" height="90" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>
		</div>
		<div class="recast-banner banner-300">
			<iframe src="http://rcm-na.amazon-adsystem.com/e/cm?t=whoshopla-20&o=1&p=12&l=ur1&category=amazonvideoondemand&banner=1B2BTR6P751PMAH984G2&f=ifr" width="300" height="250" scrolling="no" border="0" marginwidth="0" style="border:none;" frameborder="0"></iframe>
		</div>
		
		<div class="recast-submit">
			<a class="btn btn-primary btn-lg" href="/login/">Log in to vote</a> 
		</div>
		
	</form>
@stop