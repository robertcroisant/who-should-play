@section('meta')
			
		<meta property="og:site_name" content="Who Should Play" />
		<meta property="og:title" content="Edit Recast :: Movie Recast Management :: Who Should Play" />
		<title>Edit Recast :: Movie Recast Management :: Who Should Play</title>

@endsection

@section('content')

	<script type="text/javascript">
		$(document).ready(function() {
			$( ".cast" ).sortable({
				placeholder: "ui-state-highlight"
			});

			$(document).on('click',function(){
				var count = $('.character-checkbox input:checked').length;
				if(count == 0)
					$('.recast-new button').prop('disabled',true);
				else
					$('.recast-new button').prop('disabled',false);
			});
		});
	</script>
	
	<h3>{{$movie->title }}</h3>

	<form action="" method="POST" class="recast-new">
		<ul class="cast clearfix">
		@foreach($characters as $movieCharacter)
			<li class="character"><div class="character-checkbox"><input type="checkbox" id="character[{{$movieCharacter->character->id}}]" name="character[{{$movieCharacter->character->id}}]" value="2" checked="checked" /> </div> <label for="character[{{$movieCharacter->character->id}}]" class="character-profile"><img src="http://whoshouldplay.com/i/c/{{$movieCharacter->character->hash}}/medium" /></label><div class="character-details"><div class="character-name">{{$movieCharacter->character->name}}</div><div class="character-actor">{{$movieCharacter->character->actor->name}}</div></div></li>
		@endforeach
		
		@foreach($movieCharacters as $movieCharacter)
			@if(!in_array($movieCharacter->id,$movieCharacterIds))
			<li class="character"><div class="character-checkbox"><input type="checkbox" id="character[{{$movieCharacter->id}}]" name="character[{{$movieCharacter->id}}]" value="1" /> </div> <label for="character[{{$movieCharacter->id}}]" class="character-profile"><img src="http://whoshouldplay.com/i/c/{{$movieCharacter->hash}}/medium" /></label><div class="character-details"><div class="character-name">{{$movieCharacter->name}}</div><div class="character-actor">{{$movieCharacter->actor->name}}</div></div></li>
			@endif
		@endforeach
		</ul>

		<button type="submit" name="editResponse" value="yes" class="btn btn-primary btn-lg">Edit Recast</button>
	</form>
	
@stop