@section('meta')
			
		<meta property="og:site_name" content="Who Should Play" />
		<meta property="og:title" content="New Recast :: Movie Recast Management :: Who Should Play" />
		<title>New Recast :: Movie Recast Management :: Who Should Play</title>

@endsection
@section('content')

	<script type="text/javascript">
		$(document).ready(function() {
			var movieCache = {};
			$( "input[type=text]" ).autocomplete({
				minLength: 2,
				source: function( request, response ) {
					var term = request.term;
					if ( term in movieCache ) {
						data = movieCache[ term ];
						//response( movieCache[ term ] );
						response($.map(data.slice(0, 5), function (item) {
							return { label: item.title, id: item.id, image: "http://whoshouldplay.com/i/m/"+item.hash+"/ac", description: 'Movie', cast: item.cast };
						}));
					} else {
						$.getJSON( "/movie/search", request, function( data, status, xhr ) {
							movieCache[ term ] = data; 
							//response( data );
							response($.map(data.slice(0, 5), function (item) {
								return { label: item.title, id: item.id, image: "http://whoshouldplay.com/i/m/"+item.hash+"/ac", description: 'Movie', cast: item.cast };
							}));
						});
					}
				},
				select: function( event, ui ) {
					$('.cast').html('');
					$('.movie-id').val(ui.item.id);
					$.each(ui.item.cast, function(index,character){
						html = '<li class="character"><div class="character-checkbox"><input type="checkbox" id="character[' + character.id +']" name="character[' + character.id +']" value="1" /> </div> <label for="character[' + character.id +']" class="character-profile"><img src="http://whoshouldplay.com/i/c/' + character.hash +'/medium" /></label><div class="character-details"><div class="character-name">' + character.name +'</div><div class="character-actor">' + character.actor.name +'</div></div></li>';
						$('.cast').append(html);
					});
					
					$( ".cast" ).sortable({
						placeholder: "ui-state-highlight"
					});
					$( ".cast" ).disableSelection();
					/*console.log(ui.item.cast);
					$element = $(".recast-character .choice input[name='hidden-" + event.target.name + "']");
					$element.val(ui.item.id);*/
				}
				
				
				
			}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
				var inner_html = '<a><div class="list_item_container"><div class="image"><img src="' + item.image + '"></div><div class="name">' + item.label + '</div><div class="description">' + item.description + '</div></div></a>';
				return $( "<li></li>" )
					.data( "item.autocomplete", item )
					.append(inner_html)
					.appendTo( ul );
			};
			
			$(document).on('click',function(){
				var count = $('.character-checkbox input:checked').length;
				if(count == 0)
					$('.recast-new button').prop('disabled',true);
				else
					$('.recast-new button').prop('disabled',false);
			});
		});
	</script>
	
	<input type="text" name="movie" value="" class="form-control" placeholder="Movie Title" />
	
	<form action="" method="POST" class="recast-new">
		<input type="hidden" name="movie" class="movie-id" value="" />
		<ul class="cast clearfix">
		</ul>

		<button type="submit" name="createResponse" value="yes" class="btn btn-primary btn-lg" disabled="disabled">Create Recast</button>
	</form>
	
@stop