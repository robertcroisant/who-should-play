@section('meta')
			
		<meta property="og:site_name" content="Who Should Play" />
		<meta property="og:title" content="{{ $movie->title }} [{{ $movieReleaseDate->format('Y') }}] :: Movie Recast Management :: Who Should Play" />
		<title>{{ $movie->title }} [{{ $movieReleaseDate->format('Y') }}] :: Movie Recast Management :: Who Should Play</title>

@endsection

@section('highlighted')

<div class="highlighted">
	<div class="content">
		<div class="recast-movie clearfix">
			<div class="recast-movie-left">
				@if(!in_array($movie->asin,array(NULL,'')))<a href="http://www.amazon.com/gp/product/{{ $movie->asin }}/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN={{ $movie->asin }}&linkCode=as2&tag=whoshopla-20"><img class="recast-poster img-rounded" src="http://whoshouldplay.com/i/m/{{ $movie->hash }}/medium" alt="{{ $movie->title }}" /></a>@else<img class="recast-poster img-rounded" src="http://whoshouldplay.com/i/m/{{ $movie->hash }}/medium" alt="{{ $movie->title }}" />@endif
				@if(!in_array($movie->asin,array(NULL,'')))<a class="btn btn-info btn-small" href="http://www.amazon.com/gp/product/{{ $movie->asin }}/ref=as_li_tf_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN={{ $movie->asin }}&linkCode=as2&tag=whoshopla-20">Watch this movie</a>@endif 
				@if(Auth::user() && ($movieRecast->user_id == Auth::user()->id || Auth::user()->access >= 9))<a class="btn btn-warning btn-small" href="/recasts/{{$movieRecast->id}}/edit/">Edit this recast</a>@endif
			</div>
			<div class="recast-movie-details">
				<div class="recast-movie-title">
					{{ $movie->title }} @if(isset($movieReleaseDate) && $movieReleaseDate) [{{ $movieReleaseDate->format('Y') }}] @endif
				</div>
				<div class="recast-movie-overview">
					{{ $movie->overview }}
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('content')
	
	<div class="recast-banner adsense">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- WSP - Responsive -->
		<ins class="adsbygoogle wsp-responsive"
		     style="display:inline-block"
		     data-ad-client="ca-pub-0338988042910939"
		     data-ad-slot="9557691497"></ins>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>
	
	<div class="panel panel-success link-area">
		<div class="panel-heading">
			<h3 class="panel-title">Send this link to your friends to hear their responses.</h3>
		</div>
		<div class="panel-body well">http://whoshouldplay.com/r/{{ $movieRecast->hash }}/</div>
	</div>

	<form role="form" class="movie-recast" action="" method="POST" >
		
	@foreach($characters as $character)
		<div class="recast-character clearfix">
			<img class="photo img-rounded img-responsive" src="http://whoshouldplay.com/i/c/{{ $character->character->hash }}/small" />
			
			<div class="ranks">
				@foreach($character->rankVotes() as $vote)
				<div class="rank">
					<div class="name">
						<span class="count">#{{ $vote['rank'] }}</span>
						{{ $vote['actor']->name }}
					</div>
					<div class="progress">
						<span class="progress-label">{{ $vote['count'] }} vote{{ ($vote['count'] == 1 ? '' : 's')}} ({{ $vote['percentage'] }}%)</span>
						<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $vote['percentage'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $vote['percentage'] }}%">
						</div>
					</div>
				</div>
				@endforeach

			</div>

		</div>
	@endforeach
		
		
	</form>
	
	<div class="list-group">
		<a class="list-group-item active">
			Responses <span class="badge">{{ sizeof($responses) }}</span>
		</a>
		@foreach($responses as $response)
			@if($response->user_id == null)
		<a href="/s/{{$response->hash }}/" class="list-group-item"><em>Anonymous</em></a>	
			@else
		<a href="/s/{{$response->hash }}/" class="list-group-item">{{ $response->user->twitter->name }} ({{ '@'.$response->user->twitter->screen_name }})</a>	
			@endif
		@endforeach
	</div>
@stop