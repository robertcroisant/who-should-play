@section('meta')
			
		<meta property="og:site_name" content="Who Should Play" />
		<meta property="og:title" content="Movie Recast Management :: Who Should Play" />
		<title>Movie Recast Management :: Who Should Play</title>

@endsection

@section('content')
	
	<div class="movie-recast-list clearfix">
	
		@foreach($movieRecasts as $movieRecast)
			<a href="/recasts/{{ $movieRecast->id }}/"><img src="http://whoshouldplay.com/i/m/{{ $movieRecast->movie->hash }}/medium/" alt="{{ $movieRecast->movie->title }}" title="{{ $movieRecast->movie->title }}" class="img-rounded" /></a> <!--(<a href="/recasts/{{ $movieRecast->id }}/edit/">edit</a>)-->
		@endforeach
		
		<a class="new" href="/recasts/new">+</a>
	
	</div>
	
@stop