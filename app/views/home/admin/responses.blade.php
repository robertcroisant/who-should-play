@section('meta')
			
		<meta property="og:site_name" content="Who Should Play" />
		<meta property="og:title" content="Movie Recast Responses :: Who Should Play" />
		<title>Movie Recast Responses :: Who Should Play</title>

@endsection

@section('content')

	<div class="list-group">
		<a class="list-group-item active">
			Responses <span class="badge">{{ sizeof($movieRecastResponses) }}</span>
		</a>
		@foreach($movieRecastResponses as $response)
			@if($response->user_id == null || !$response->user)
		<a href="/s/{{$response->hash }}/" class="list-group-item">{{ $response->recast->movie->title }} :: <em>Anonymous</em> :: {{ $response->created_at }}</a>	
			@else
		<a href="/s/{{$response->hash }}/" class="list-group-item">{{ $response->recast->movie->title }} :: {{ $response->user->twitter->name }} ({{ '@'.$response->user->twitter->screen_name }}) :: {{ $response->created_at }}</a>	
			@endif
		
		@endforeach
	</div>
	
@stop