
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Who Should Play: You play the casting agent for your favorite movies It’s easy. After logging in, start picking which actor you would have cast in these iconic roles, submit your choices and share your picks!">
		<meta name="keywords" content="Who Should Play, Movie Recasts, Movies, Films">
		<meta name="author" content="">
		<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

		@yield('meta')
		

		<link href="/css/core/styles.css" rel="stylesheet">
		<link href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet">
		<!-- Bootstrap core CSS -->
		<link href="/css/bootstrap/bootstrap.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="/css/bootstrap/jumbotron-narrow.css" rel="stylesheet">
		
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	</head>

	<body>
		<div class="message-container alert-warning{{ Session::get('message-type') }}"@if(Session::has('message')) style="display: block;"@endif>
			@if(TRUE || Session::has('message'))
			<div class="alert alert-dismissable">
				<div class="content">
					<button type="button" class="close" data-target=".alert" data-dismiss="alert" aria-hidden="true">&times;</button>

					@if(Session::get('message-type') == 'success')
					<strong>Success:</strong> 
					@elseif(Session::get('message-type') == 'info')
					<strong>Info:</strong> 
					@elseif(Session::get('message-type') == 'warning')
					<strong>Warning:</strong> 
					@elseif(Session::get('message-type') == 'danger')
					<strong>Error:</strong> 
					@endif
					{{ Session::get('message') }}
				</div>
			</div>
			@endif
		</div>
		<div class="header-container">
			<div class="header">
				<ul class="nav nav-pills pull-left">
					<li><a class="glyph-container refresh" href="/random/"><span class="glyphicon glyphicon-refresh"></span></a></li>
				</ul>
				<span class="navbar-brand">Who Should Play</span>
				<ul class="nav nav-pills pull-right">
					
					<?php if(Auth::check()) echo '<li><a href="/recasts/" class="admin">My Recasts</a></li>'; ?>
					
					<li><a class="glyph-container home" href="/"><span class="glyphicon glyphicon-home"></span></a></li>
					
					<?php if(!Auth::check()){ ?>
						<li><a class="glyph-container login" href="/login/"><span class="glyphicon glyphicon-log-in"></span></a></li>
					<? } else { ?>
						<li><a class="glyph-container login" href="/logout/"><span class="glyphicon glyphicon-log-out"></span></a></li>
					<?php } ?>
				</ul>
				<h3 class="text-muted">&nbsp;</h3>
			</div>
		</div>
		
		@if(Auth::check() == false && !Session::has('shown-modal') && (Session::put('shown-modal',true) || true))
		<script type="text/javascript">
			$(window).load(function(){
				$('#myModal').modal('show');
			});
		</script>
		@endif
		<!-- Modal -->
		<div class="intro-modal modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Who Should Play</h4>
					</div>
					<div class="modal-body">
						<p>It's easy. Just start picking which actor you would have cast in these iconic roles, submit your choices and share your picks with your friends!</p>
	
						<p>Or, if you’d like to see who your friends pick, simply log in with twitter, choose a movie, pick the characters that need recasting, and share the link with your friends. Their results will be available at "My Recasts"</p>
						
						<p>Have fun!</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">I'll log in later</button>
						<a class="btn btn-primary" href="/login/">Log me in using twitter!</a>
					</div>
				</div>
			</div>
		</div>
		
		@yield('highlighted')
		
		<div class="container">
			
			<div class="content">
				@yield('content')
			</div>

		</div>
		
		<div class="footer-container">
			<div class="footer">
				<span class="copyright">&copy; Who Should Play <?php echo date('Y'); ?></span>
				<?php if(Auth::check() && Auth::user()->access >= 9) echo '<a href="/admin/" class="admin">Admin</a>'; ?>
			</div>
		</div>


		<!-- Bootstrap core JavaScript
		================================================== -->
		
		<script src="/js/bootstrap/bootstrap.min.js"></script>
		<script src="/js/core/script.js"></script>
		<!-- Placed at the end of the document so the pages load faster -->
		<!-- Piwik -->
		<script type="text/javascript" defer="defer">
			var _paq = _paq || [];
			_paq.push(['trackPageView']);
			_paq.push(['enableLinkTracking']);
			@if(isset($doRecastGoal) && $doRecastGoal)
			
				_paq.push(['trackGoal', 1]);
				
			@endif
			
			(function() {
				var u=(("https:" == document.location.protocol) ? "https" : "http") + "://piwik.whoshouldplay.com/";
				_paq.push(['setTrackerUrl', u+'piwik.php']);
				_paq.push(['setSiteId', 1]);
				var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
				g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
			})();
		
		</script>
		<noscript><p><img src="http://piwik.whoshouldplay.com/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
		<!-- End Piwik Code -->
	</body>
</html>
