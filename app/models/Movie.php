<?php

class Movie extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movies';
	
	
	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	
	
	public function cast(){
		return $this->hasMany('MovieCharacter');
	}
	
	public function generateHash(){

		$min = 4;
		$max = 15;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$length = mt_rand($min,$max);
		$hash = '';
		for ($i = 0; $i < $length; $i++) {
			$hash .= $characters[rand(0, strlen($characters) - 1)];
		}
		
		$responses = Movie::where('hash','=',$hash);
		while($responses->count() > 0){
			$length = mt_rand($min,$max);
			$hash = '';
			for ($i = 0; $i < $length; $i++) {
				$hash .= $characters[rand(0, strlen($characters) - 1)];
			}
		}
		
		return $hash;
		
	}
	
	
	public function getFromTMDBVoteCount($vote_count){
		
		$api_key = 'a3dc111e66105f6387e99393813ae4d5';
		
		$continue = true;
		$page = 1;
		while($continue){
			
			
			$url = "https://api.themoviedb.org/3/discover/movie?api_key=$api_key&vote_count.gte=$vote_count&page=$page";
			
			$curl = new Curl;
			
			$curl->create($url);
			$curl->get();
			$json = $curl->execute();
			
			$movie_array = json_decode($json,true);
			
			foreach($movie_array['results'] as $result){
				$movie_id = $result['id'];
				
				$this->getFromTMDB($movie_id);
			}
			
			$page++;
			
			if(isset($movie_array['total_pages']) && isset($movie_array['results']) && sizeof($movie_array['results']) > 0 && $page <= $movie_array['total_pages'])
				$continue = true;
			else
				$continue = false;
			
		}
		
		
	}
	
	
	public function getFromTMDB($movie_id){
		
		if(Movie::where('tmdb_id','=',$movie_id)->count() == 0){
			
			$api_key = 'a3dc111e66105f6387e99393813ae4d5';
			$url = "https://api.themoviedb.org/3/movie/$movie_id?api_key=$api_key";
			
			$curl = new Curl;
			sleep(1);
			$curl->create($url);
			$curl->get();
			$json = $curl->execute();
			
			$movie_array = json_decode($json,true);
			//print_r($movie_array);
			
			
			if(isset($movie_array['id'])){
				
				if (!file_exists(public_path().'/images/movies/'.$movie_array['id'])) {
					mkdir(public_path().'/images/movies/'.$movie_array['id'], 0777, true);
				}
	
				$file = public_path().'/images/movies/'.$movie_array['id'].'/backdrop.jpg';
				$file_remote = 'http://image.tmdb.org/t/p/original' .$movie_array['backdrop_path'];
				$headers = array();
				
				$ch = curl_init($file_remote);
				$fp = fopen($file, 'wb');
				curl_setopt($ch, CURLOPT_FILE, $fp);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_FTP_CREATE_MISSING_DIRS, true);
				curl_exec($ch);
				curl_close($ch);
				fclose($fp);
				
				$file = public_path().'/images/movies/'.$movie_array['id'].'/poster.jpg';
				$file_remote = 'http://image.tmdb.org/t/p/original' .$movie_array['poster_path'];
				$headers = array();
				
				$ch = curl_init($file_remote);
				$fp = fopen($file, 'wb');
				curl_setopt($ch, CURLOPT_FILE, $fp);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_FTP_CREATE_MISSING_DIRS, true);
				curl_exec($ch);
				curl_close($ch);
				fclose($fp);
	
				$backdrop_link = 'http://whoshouldplay.robertcroisant.com/images/movies/'.$movie_array['id'].'/backdrop.jpg';
				$poster_link = 'http://whoshouldplay.robertcroisant.com/images/movies/'.$movie_array['id'].'/poster.jpg';
				
				$movie = new Movie;
				$movie->title = $movie_array['title'];
				$movie->hash = $movie->generateHash();
				$movie->imdb_id = $movie_array['imdb_id'];
				$movie->tmdb_id = $movie_array['id'];
				$movie->backdrop_link = $backdrop_link;
				$movie->poster_link = $poster_link;
				$genres = array();
				foreach($movie_array['genres'] as $genre_array){
						$genres[] = $genre_array['name'];
				}
				$movie->overview = $movie_array['overview'];
				$movie->genres = implode(',',$genres);
				$movie->release_date = $movie_array['release_date'];
				$movie->revenue = $movie_array['revenue'];
				$movie->runtime = $movie_array['runtime'];
				$movie->tagline = $movie_array['tagline'];
				$movie->save();
				
				
				$url = "https://api.themoviedb.org/3/movie/$movie_id/credits?api_key=$api_key";
				
				$curl = new Curl;
				
				$curl->create($url);
				$curl->get();
				$json = $curl->execute();
				
				$movie_cast_array = json_decode($json,true);

				foreach($movie_cast_array['cast'] as $cast){
					
					$people_id = $cast['id'];
					
					$actor = Actor::where('tmdb_id','=',$people_id)->first();
					if(!$actor){
						
						$url = "https://api.themoviedb.org/3/person/$people_id?api_key=$api_key";
						$curl = new Curl;
						
						$curl->create($url);
						$curl->get();
						$json = $curl->execute();
						
						$actor_array = json_decode($json,true);
						
						if (!file_exists(public_path().'/images/actors/'.$actor_array['id'])) {
							mkdir(public_path().'/images/actors/'.$actor_array['id'], 0777, true);
						}
			
						$file = public_path().'/images/actors/'.$actor_array['id'].'/profile.jpg';
						$file_remote = 'http://image.tmdb.org/t/p/original' .$actor_array['profile_path'];
						$headers = array();
						
						$ch = curl_init($file_remote);
						$fp = fopen($file, 'wb');
						curl_setopt($ch, CURLOPT_FILE, $fp);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_FTP_CREATE_MISSING_DIRS, true);
						curl_exec($ch);
						curl_close($ch);
						fclose($fp);
						
						$actor = new Actor;
						$actor->name = $actor_array['name'];
						$actor->hash = $actor->generateHash();
						$actor->imdb_id = '';
						$actor->tmdb_id = $people_id;
						$actor->profile_link = 'http://whoshouldplay.robertcroisant.com/images/actors/'.$actor_array['id'].'/profile.jpg';
						$actor->birthday = $actor_array['birthday'];
						$actor->deathday = $actor_array['deathday'];
						$actor->homepage = $actor_array['homepage'];
						$actor->place_of_birth = $actor_array['place_of_birth'];
						$actor->save();
						
					}
					
					if($cast['character'] != '' && $actor->id){
						
						$character = new MovieCharacter;
						$character->name = $cast['character'];
						$character->hash = $character->generateHash();
						$character->movie_id = $movie->id;
						$character->actor_id = $actor->id;
						$character->profile_link = $actor->profile_link;
						$character->save();
					
					}
					
				}
			
			} else {
				return false;
			}
			
		} else {
			
			return false;
			
		}
		
			
	}

}