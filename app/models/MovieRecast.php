<?php

class MovieRecast extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movie_recasts';
	
	
	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	
	public function generateHash(){

		$min = 4;
		$max = 10;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$length = mt_rand($min,$max);
		$hash = '';
		for ($i = 0; $i < $length; $i++) {
			$hash .= $characters[rand(0, strlen($characters) - 1)];
		}
		
		$responses = MovieRecast::where('hash','=',$hash);
		while($responses->count() > 0){
			$length = mt_rand($min,$max);
			$hash = '';
			for ($i = 0; $i < $length; $i++) {
				$hash .= $characters[rand(0, strlen($characters) - 1)];
			}
		}
		
		return $hash;
		
	}
	
	public function movie(){
		return $this->belongsTo('Movie');
	}
	
	public function recastCharacters(){
		return $this->hasMany('MovieRecastCharacter')->orderby('movie_recast_characters.order','ASC');
	}
	
	public function responses(){
		return $this->hasMany('MovieRecastResponse');
	}
	
	public function hasVoted(User $user){
		if($user){
			$movieRecastResponses = MovieRecastResponse::where('movie_recast_id','=',$this->id)->where('user_id','=',$user->id)->count();
		} else {
			$movieRecastResponses = MovieRecastResponse::where('movie_recast_id','=',$this->id)->where('session_id','=',Session::getId())->count();
		}

		return ($movieRecastResponses > 0);
		
	}
	
	public function getUserResponse($user){
		if($user){
			return MovieRecastResponse::where('movie_recast_id','=',$this->id)->where('user_id','=',$user->id)->first();
		} else {
			return MovieRecastResponse::where('movie_recast_id','=',$this->id)->where('session_id','=',Session::getId())->first();
		}
	}

}