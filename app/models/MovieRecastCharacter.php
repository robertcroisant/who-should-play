<?php

class MovieRecastCharacter extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movie_recast_characters';
	
	
	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	
	
	public function recast(){
		return $this->belongsTo('MovieRecast','movie_recast_id');
	}
	
	public function character(){
		return $this->belongsTo('MovieCharacter','movie_character_id');
	}
	
	public function votes(){
		return $this->hasMany('MovieRecastResponseVote');
	}
	
	public function compareVotes($a,$b){
		if($a['count'] == $b['count'])
			return 0;
		
		return ($a['count'] < $b['count']) ? 1 : -1;
	}
	
	public function rankVotes(){
		
		
		
		$votes = $this->votes;
		$voteCount = sizeof($votes);
		
		$rankedVotes = array();
		foreach($votes as $vote){
			if(isset($rankedVotes[$vote->actor_id])){
				$rankedVotes[$vote->actor_id]['count']++;
			} else {
				$rankedVotes[$vote->actor_id] = array(
					'count' => 1,
					'actor' => $vote->actor
				);
			}
		}
		uasort($rankedVotes,'MovieRecastCharacter::compareVotes');
		$rank = 1;
		foreach($rankedVotes as $key => $vote){
			$rankedVotes[$key]['rank'] = ($rank);
			$rankedVotes[$key]['percentage'] = 100*($vote['count']/$voteCount);
			$rank++;
		}
		return $rankedVotes;
	}

}