<?php

class MovieRecastResponse extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movie_recast_responses';
	
	
	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	
	
	public function recast(){
		return $this->belongsTo('MovieRecast','movie_recast_id');
	}
	
	public function user(){
		return $this->belongsTo('User');
	}
	
	public function votes(){
		return $this->hasMany('MovieRecastResponseVote');	
	}
	
	public function getCharacterVote($movieRecastCharacter){
		return MovieRecastResponseVote::where('movie_recast_response_id','=',$this->id)->where('movie_recast_character_id','=',$movieRecastCharacter->id)->first();
	}
	
	public function generateHash(){

		$min = 4;
		$max = 15;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$length = mt_rand($min,$max);
		$hash = '';
		for ($i = 0; $i < $length; $i++) {
			$hash .= $characters[rand(0, strlen($characters) - 1)];
		}
		
		$responses = MovieRecastResponse::where('hash','=',$hash);
		while($responses->count() > 0){
			$length = mt_rand($min,$max);
			$hash = '';
			for ($i = 0; $i < $length; $i++) {
				$hash .= $characters[rand(0, strlen($characters) - 1)];
			}
		}
		
		return $hash;
		
	}

}