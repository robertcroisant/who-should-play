<?php

class MovieCharacter extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movie_characters';
	
	
	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	
	
	public function movie(){
		return $this->belongsTo('Movie');
	}
	
	public function actor(){
		return $this->belongsTo('Actor');
	}
	
	public function generateHash(){

		$min = 4;
		$max = 15;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$length = mt_rand($min,$max);
		$hash = '';
		for ($i = 0; $i < $length; $i++) {
			$hash .= $characters[rand(0, strlen($characters) - 1)];
		}
		
		$responses = MovieCharacter::where('hash','=',$hash);
		while($responses->count() > 0){
			$length = mt_rand($min,$max);
			$hash = '';
			for ($i = 0; $i < $length; $i++) {
				$hash .= $characters[rand(0, strlen($characters) - 1)];
			}
		}
		
		return $hash;
		
	}
	
}