<?php

class MovieRecastResponseVote extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movie_recast_response_votes';
	
	
	protected $softDelete = true;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();
	
	
	public function response(){
		return $this->belongsTo('MovieRecastResponse','movie_recast_response_id');
	}
	
	public function character(){
		return $this->belongsTo('MovieRecastCharacter','movie_recast_character_id');
	}
	
	public function actor(){
		return $this->belongsTo('Actor');
	}

}