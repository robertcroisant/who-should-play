<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($t) {
				$t->engine = 'InnoDB';
				$t->increments('id');
				$t->string('email', 100)->nullable();
				$t->string('password', 64)->nullable();
				$t->integer('access')->default(1);
				$t->timestamps();
				$t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}