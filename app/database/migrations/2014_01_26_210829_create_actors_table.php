<?php

use Illuminate\Database\Migrations\Migration;

class CreateActorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('actors', function($t) {
				$t->engine = 'InnoDB';
				$t->integer('id',true);
				$t->string('name', 255);
				$t->string('hash', 50)->nullable();
				$t->string('imdb_id',50)->nullable();
				$t->integer('tmdb_id')->nullable();
				$t->string('profile_link',255)->nullable();
				$t->string('birthday',255)->nullable();
				$t->string('deathday',255)->nullable();
				$t->string('homepage',255)->nullable();
				$t->string('place_of_birth',255)->nullable();
				$t->timestamps();
				$t->softDeletes();
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('actors');
	}

}