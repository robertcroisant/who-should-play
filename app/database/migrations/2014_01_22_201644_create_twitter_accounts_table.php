<?php

use Illuminate\Database\Migrations\Migration;

class CreateTwitterAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('twitter_accounts', function($t) {
				$t->engine = 'InnoDB';
				$t->integer('id',true);
				$t->string('name', 255);
				$t->string('screen_name', 100);
				$t->string('location', 255)->nullable();
				$t->text('description')->nullable();
				$t->string('url', 255)->nullable();
				$t->integer('followers_count')->nullable();
				$t->integer('statuses_count')->nullable();
				$t->string('lang', 5)->nullable();
				$t->string('time_zone', 100)->nullable();
				$t->string('twitter_created', 200)->nullable();
				$t->timestamps();
				$t->softDeletes();
		});
		
		Schema::table('users', function($t){
			$t->integer('twitter_id')->after('password');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function($t){
			$t->dropColumn('twitter_id');
		});
		Schema::drop('twitter_accounts');
	}

}