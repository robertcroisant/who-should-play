<?php

use Illuminate\Database\Migrations\Migration;

class CreateMovieRecastTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movie_recasts', function($t) {
				$t->engine = 'InnoDB';
				$t->integer('id',true);
				$t->integer('movie_id');
				$t->integer('user_id');
				$t->integer('is_public');
				$t->timestamps();
				$t->softDeletes();
		});
		
		Schema::create('movie_recast_characters', function($t) {
				$t->engine = 'InnoDB';
				$t->integer('id',true);
				$t->integer('movie_recast_id');
				$t->integer('movie_character_id');
				$t->integer('order');
				$t->timestamps();
				$t->softDeletes();
		});
		
		Schema::create('movie_recast_responses', function($t) {
				$t->engine = 'InnoDB';
				$t->integer('id',true);
				$t->integer('movie_recast_id');
				$t->integer('user_id');
				$t->string('hash', 50)->nullable();
				$t->timestamps();
				$t->softDeletes();
		});
		
		Schema::create('movie_recast_response_votes', function($t) {
				$t->engine = 'InnoDB';
				$t->integer('id',true);
				$t->integer('movie_recast_response_id');
				$t->integer('movie_recast_character_id');
				$t->integer('actor_id');
				$t->timestamps();
				$t->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movie_recast_votes');
		Schema::drop('movie_recast_characters');
		Schema::drop('movie_recasts');
	}

}