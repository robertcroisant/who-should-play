<?php

use Illuminate\Database\Migrations\Migration;

class CreateMovieCharactersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movie_characters', function($t) {
				$t->engine = 'InnoDB';
				$t->integer('id',true);
				$t->string('name', 255);
				$t->string('hash', 50)->nullable();
				$t->integer('movie_id');
				$t->integer('actor_id');
				$t->string('profile_link',255)->nullable();
				$t->timestamps();
				$t->softDeletes();
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movie_characters');
	}

}