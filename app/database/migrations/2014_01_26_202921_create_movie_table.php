<?php

use Illuminate\Database\Migrations\Migration;

class CreateMovieTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movies', function($t) {
				$t->engine = 'InnoDB';
				$t->integer('id',true);
				$t->string('title', 255);
				$t->string('hash', 50)->nullable();
				$t->string('imdb_id',50)->nullable();
				$t->integer('tmdb_id')->nullable();
				$t->string('backdrop_link',255)->nullable();
				$t->string('poster_link',255)->nullable();
				$t->text('overview')->nullable();
				$t->string('genres',255)->nullable();
				$t->string('release_date',20)->nullable();
				$t->integer('revenue')->nullable();
				$t->integer('runtime')->nullable();
				$t->string('tagline',500)->nullable();
				$t->string('asin',30)->nullable();
				$t->timestamps();
				$t->softDeletes();
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movies');
	}

}